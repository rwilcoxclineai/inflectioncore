"""inflectionai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path,include
from django.conf import settings
from django.conf.urls.static import static
from customauth.views import *
from core.views import *
from machina import urls as machina_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^accounts/', include('allauth.urls')),
    re_path(r'^$', home, name="home"),

    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),

    re_path(r'^accounts/profile/$', user_profile, name='account_profile'),

    re_path(r'^accounts/verify/$', teacher_student_verification, name='teacher_student_verification'),

    re_path(r'^accounts/profile/verify/$', verify_teacher, name='verify_teacher'),

    re_path(r'^accounts/profile/add-course/$', student_add_course, name='student-add-course'),

    path('questions/<int:id>/', single_question, name="single_question"),

 	 path('accounts/profile/<str:course_uuid>/', course_detail),
     path('accounts/profile/student/<str:course_uuid>/', course_detail_student),


     path('accounts/profile/<str:course_uuid>/subscribe/', teacher_subscribe_course),





   

     path('forum/', include(machina_urls)),
     
    
     re_path(r'^vocabulary/$', vocabulary, name='vocabulary'),
     path('vocabulary/<slug:slug>/',lesson_vocabulary, name='lesson_vocabulary'),
     re_path(r'^courses/$', teacher_student_modules, name='teacher_student_modules'),

     path('course/<str:slug>/', module_topic_list, name="module_topic_list"),
     path('course/<slug:course_slug>/<slug:topic_slug>/', module_lesson_list, name="module_lesson_list"),
     path('course/<slug:course_slug>/<slug:topic_slug>/pre-assessment/<int:id>', module_pre_assessment, name="module_pre_assessment"),
     path('course/<slug:course_slug>/<slug:topic_slug>/post-assessment/<int:id>', module_post_assessment, name="module_post_assessment"),

     path('course/<slug:course_slug>/<slug:topic_slug>/<slug:lesson_slug>/<slug:lessoncontent_slug>', module_lesson, name="module_lesson"),
     

     re_path(r'^students/products/$',  products_list, name='products_list'),
     path('students/products/<slug:slug>/',product_module_list, name='product_module_list'),
     path('students/products/<slug:product_slug>/<slug:course_slug>/classes',product_module_class_list, name='product_module_class_list'),
     path('<slug:slug>/students',class_students_list, name='class_students_list'),


     path('statistics/<slug:slug>',dashboard_single_student_stats, name='dashboard_single_student_stats'),
     path('statistics/<slug:user_slug>/<slug:course_slug>',dashboard_single_student_topic_list, name='dashboard_single_student_topic_list'),
  
     path('statistics/<slug:user_slug>/<slug:course_slug>/<slug:topic_slug>',dashboard_single_student_lessons_list, name='dashboard_single_student_lessons_list'),


    #DASHBOARD URLS
    path('dashboard/', dashboard,name='dashboard'),
    path('dashboard/<str:slug>/', dashboard_topic_list, name="dashboard_topic_list"),
    path('dashboard/<slug:course_slug>/<slug:topic_slug>/', dashboard_lesson_list, name="dashboard_lesson_list"),

     re_path(r'^api-auth/', include('rest_framework.urls')),
     re_path(r'^api/core/', include("core.api.urls")),


     re_path(r'^ajax/get-similar-words/$', ajax_similar_word_rankings, name='get-similar-words'),
     re_path(r'^ajax/get-ner-type/$', ajax_ner_model, name='get-ner-type'), 
     re_path(r'^ajax/get-pos-type/$', ajax_pos_model, name='get-pos-type'), 

     re_path(r'^ajax/get-sim-to-words/$', ajax_sim_to_words, name='get-sim-to-words'),

     re_path(r'^ajax/word-addition/$', ajax_word_addition, name='word-addition'), 

     re_path(r'^ajax/word-to-plot/$', ajax_word_to_plot, name='word-to-plot'), 
   

   
   
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
