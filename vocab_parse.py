import csv
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'inflectiontest.settings'
django.setup()


from core.models import *
csv.register_dialect('myDialect',
delimiter = ';',
skipinitialspace=True)

lesson_1 = "introduction-to-textual-data"
lesson_2 = "text-messages-an-example-of-textual-data"
lesson_3 = "introduction-to-natural-language-processing"
lesson_4 = "training-natural-language-processing-models"
lesson_5 = "using-natural-language-processing-models"


with open('T1_Vocab_V3.csv') as csv_file:
	reader = csv.reader(csv_file, dialect='myDialect')

	for row in reader:


	
		lesson_number = row[2]
		dict_word = row[3]
		try:
			vocab = Vocabulary.objects.get(word=dict_word)
			instance = vocab
			if lesson_number == "1":
				print(lesson_1)
				instance.lesson = Lesson.objects.get(slug = lesson_1)
				instance.save()

			elif lesson_number == "2":
				print(lesson_2)
				instance.lesson = Lesson.objects.get(slug = lesson_2)
				instance.save()

			elif lesson_number == "3":
				print(lesson_3)
				instance.lesson = Lesson.objects.get(slug = lesson_3)
				instance.save()

			elif lesson_number == "4":
				print(lesson_4)
				instance.lesson = Lesson.objects.get(slug = lesson_4)
				instance.save()

			elif lesson_number == "5":
				print(lesson_5)
				instance.lesson = Lesson.objects.get(slug = lesson_5)
				instance.save()
		except Exception as e:
			print("Error",e, dict_word)


