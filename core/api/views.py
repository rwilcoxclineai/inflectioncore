from rest_framework.decorators import api_view
from rest_framework.response import Response
import glob as glob
import json
from ..models import *
import os
from django.conf import settings
import gensim as gensim

MEDIA_ROOT = os.path.join(settings.BASE_DIR, "nlp_models")
models_list = sorted(glob.glob(MEDIA_ROOT+'/*.bin'))
model_name = models_list[-1]
model = gensim.models.KeyedVectors.load_word2vec_format(model_name, binary=True)


def similar_word_rankings(a, n=10):
    sims = model.most_similar(a, topn = n)
    dic = dict(sims)
    similar_words = {k: v for k, v in dic.items() if not k.startswith("wiki")}
    similar_words = {k: similar_words[k] for k in list(similar_words)[:n]}
    return similar_words

@api_view(['GET', 'POST'])
def hello_world(request):
    if request.method == 'POST':

        raw_data = request.data['input']
        print(raw_data)
        sims = similar_word_rankings('Donald_Trump')

        return Response({"message": "Got some data!", "data": sims})
    return Response({"message": "Hello, world!"})
