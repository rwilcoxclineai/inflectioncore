
def get_lesson_question_count(topic):

	question_count_list = []

	for lesson in topic.lesson_set.all():
		lesson_content = lesson.lessoncontent_set.all()
		lesson_content = lesson_content.exclude(question__isnull=True)
		question_count_list.append(lesson_content.count())

	return question_count_list

