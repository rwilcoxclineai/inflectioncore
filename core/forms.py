
from django import forms
from django.forms import fields, models, formsets, widgets,ModelForm ,modelformset_factory

from .models import *
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField
User = get_user_model()

class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('first_name', 'email', 'password', 'is_active')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
    

    class Meta:
        model = User
        fields = ('first_name','last_name', 'email',) #'full_name',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserResponseForm(forms.Form):
	question_id = forms.IntegerField()
	answer_id = forms.IntegerField()


class UserTextFreeForm(forms.Form):
	question_id = forms.IntegerField()
	my_answer = forms.CharField(widget=forms.Textarea)


class TestFileForm(ModelForm):
	class Meta:
		model = TestFile
		exclude = ['user_file_text','file_name']
		



#---------


class SignupForm(forms.ModelForm):
	   # teacher_student = forms.CharField(max_length=10,label='phone_number')
	PERSON_TYPE_CHOICES = (
	    ('Teacher', ('Teacher')),
	    ('Student', ('Student ')),
	)
	teacher_student = forms.ChoiceField(choices=PERSON_TYPE_CHOICES, widget=forms.RadioSelect())
	class Meta:
		model = User
		fields = ['email','teacher_student']


 

	def signup(self, request, user):

		user.teacher_student = self.cleaned_data['teacher_student']
		user.save()




class TeacherVerificationForm(forms.Form):

	school_name = forms.CharField(label='School Name', max_length=500)
	school_uuid = forms.CharField(label='School ID',   max_length=500)


class StudentAddCourseForm(forms.Form):
	teacher_email =  forms.EmailField(label='Teacher Email')
	course_uuid = forms.CharField(label = 'Course ID')



class NLPModelForm(forms.Form):
    model_name =  forms.CharField(label='Enter the model name to be used')
    word = forms.CharField(label = 'Enter a word')

