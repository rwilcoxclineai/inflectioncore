from django.shortcuts import render

# Create your views here.
from django.shortcuts import get_object_or_404,redirect
from django.contrib import messages
from .models import *

from .forms import UserTextFreeForm,NLPModelForm
from .helper_functions import *
from django.db.models import Count,Sum
import math
# def teacher_dashboard(request,username):
# 	template = "teacher_dashboard.html"
# 	context = {}
# 	query = request.GET.get('q', None)
# 	print(query)
# 	if query is not None:
# 		print("We got some query",query)


# 		students = StudentProfile.objects.filter(user__email = query)
# 		print ("Students qs is", students)
# 		if students.count() == 0:
# 			students = []
# 		else:
# 			context["students"] = students

# 	else:
# 		print("No Query")


# 	context ["query"] = query


# 	return render(request,template,context)



def teacher_search_query(request):

	template = "teacher_search_query.html"

	context = {}

	return render(request,template,context)


def vocabulary(request):
	template = "vocabulary.html"
	vocabulary = Vocabulary.objects.all()
	teacher_profile = None
	student_profile = None

	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	if teacher_profile and not teacher_profile.verified:
		return redirect("account_profile")
	elif student_profile and student_profile.course.all().count() == 0:
		return redirect("account_profile")


	
	context = {
	"vocabulary":vocabulary,
	"teacher_profile":teacher_profile,
	"student_profile":student_profile,
	
	}

	return render(request,template,context)



def teacher_student_modules(request):
	template = "teacher_student_modules.html"
	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	if teacher_profile and not teacher_profile.verified:
		return redirect("account_profile")
	elif student_profile and student_profile.course.all().count() == 0:
		return redirect("account_profile")

	topics_completed_list = 0
	if request.user.teacher_student == "Student":
		for student_course in student_profile.course.all():
			course_topics = student_course.topic_set.all()
			for t in course_topics:
				try:
					if UserLessonCompletionStatus.objects.filter(topic__slug = t.slug,student_profile = student_profile).first().topic_completed:

						topics_completed_list = topics_completed_list + 1
				except:
					pass
	mod_completion_list = []
	if request.user.teacher_student == "Teacher":
		teacher_students = teacher_profile.studentprofile_set.all()
		students_emails = teacher_students.values_list('user__email',flat=True)
		qs_res =UserLessonCompletionStatus.objects.filter(student_profile__user__email__in = students_emails,
														topic_completed=True).values('student_profile__user__email',
																						'course__name','topic__name').distinct()

		for course in teacher_profile.course.all():
			count = 0
			for data in qs_res:
				if data['course__name'] == course.name:
					count = count + 1
			
			mod_completion_list.append(count)








		# for course in teacher_profile.course.all():
		# 	course_topics  = course.topic_set.all()

		# 	for topic in course_topics:
		# 		topic_count = 0
		# 		topic_lesson_count = topic.lesson_set.all().count()
		# 		for student in teacher_students:

		# 			students_count = UserLessonCompletionStatus.objects.filter(student_profile = student,topic=topic,topic_completed = True).count()
		# 			mod_completion_list.append(students_count)
				
	
	context = {
	"teacher_profile":teacher_profile,
	"student_profile":student_profile,
	"topics_completed_list":topics_completed_list,
	"mod_completion_list":mod_completion_list,
	}
	return render(request,template,context)


def module_topic_list(request,slug):
	template = "module_topic_list.html"
	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	course = Course.objects.get(slug = slug)

	# topics = course.topic_set.all()
	# for topic in topics:

	# qs = UserLessonCompletionStatus.objects.values('lesson__topic__name',unique=True).annotate(topic_completed=Count('student_profile', 
	# 	filter=Q(topic_completed=True)))


	

	topics = Topic.objects.filter(course__slug = slug)
	student_topic_completion_list = []

	for topic in topics:
		count = 0
		try:
			teacher_students  = teacher_profile.studentprofile_set.all()
		except:
			teacher_students = []
		for student in teacher_students:

			student_count = UserLessonCompletionStatus.objects.filter(student_profile = student,topic = topic,topic_completed=True).count()
			if student_count > 1:
				count = count + 1
			print("Student is", student, "count is student_count")
		student_topic_completion_list.append(count)







	context = {
		"course":course,
		"teacher_profile":teacher_profile,
		"student_profile":student_profile,
		"student_topic_completion_list":student_topic_completion_list,

	}

	return render(request,template,context)


def module_lesson_list(request,course_slug,topic_slug):
	template = "module_lesson_list.html"
	teacher_profile = None
	student_profile = None
	topic = Topic.objects.get(slug = topic_slug)






	user_pre_assessment_status = None
	user_post_assessment_status = None

	try:
		instance = UserPreAssessmentStatus.objects.get(user=request.user,topic=topic)
		user_pre_assessment_status = instance.completed
	except:
		user_pre_assessment_status = None

	try:
		post_instance = UserPostAssessmentStatus.objects.get(user=request.user,topic=topic)
		user_post_assessment_status = post_instance.completed
	except:
		user_post_assessment_status = None


	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")

	try:
		user_lesson_completion_status_instance = UserLessonCompletionStatus.objects.filter(topic__slug = topic_slug,student_profile=student_profile)
		print("USER LESSON COMPLETION STATUS QS",user_lesson_completion_status_instance )

		if topic.lesson_set.all().count() == user_lesson_completion_status_instance.count():
			user_lesson_completion_status_instance.update(topic_completed = True)
	except:
		print("ERror")
	
	lessons = topic.lesson_set.all()

	lesson_completion_list = []
	for lesson in lessons:

		request.session[lesson.slug] = 1
		try:
			lesson_status = UserLessonCompletionStatus.objects.get(lesson__slug = lesson.slug,student_profile=student_profile)
			if lesson_status:
				lesson_completion_list.append("1")
			else:
				lesson_completion_list.append("0")
		except:
			lesson_completion_list.append("0")

	completed_student_lessons_list = None
	if student_profile is None:
	#how many students completd a certain lesson code

		teacher_students = teacher_profile.studentprofile_set.all()

		completed_student_lessons_list = []
		for lesson in lessons:
			count = 0
			for student in teacher_students:
				print("Currently the student is ", student)
				completed_lessons = UserLessonCompletionStatus.objects.filter(student_profile = student,lesson__slug = lesson.slug,lesson_completed = True)
				print("The count of completed lessons is ", completed_lessons.count())
			# if completed_lessons.student_profile in teacher_profile.studentprofile_set.all():
			# teacher_student_ids = teacher_profile.studentprofile_set.all().values_list('user__id',flat=True)
			# completed_lesson_student_ids = completed_lessons.values_list('student_profile__user__id',flat=True)


				count = count + completed_lessons.count()
			#previously used this -- which did not account for the teacher, gave the wrong count
			completed_student_lessons_list.append(count)
			#new piece of line
			# completed_student_lessons_list.append(completed_lessons.count() -  len(set(completed_lesson_student_ids) - set(teacher_student_ids)) )
			# else:
				# completed_student_lessons_list.append(0)



		print("completed_student_lessons_list",completed_student_lessons_list)
		print("lesson_completion_list",lesson_completion_list)




	user_topic_completion_status = False
	topic_status  = UserLessonCompletionStatus.objects.filter(student_profile = student_profile, topic_completed=True)
	if topic_status.count() > 0:
		user_topic_completion_status = True

	


	
	question_count_list = get_lesson_question_count(topic)
	print("Question count list is", question_count_list)
	context = {
		"topic":topic,
		"teacher_profile":teacher_profile,
		"student_profile": student_profile,
		"question_count_list":question_count_list,
		"lesson_completion_list":lesson_completion_list,
		"completed_student_lessons_list":completed_student_lessons_list,
		"user_pre_assessment_status":user_pre_assessment_status,
		"user_topic_completion_status":user_topic_completion_status,
		"user_post_assessment_status":user_post_assessment_status,

	}
	return render(request,template,context)



def module_pre_assessment(request,id,course_slug,topic_slug):

	template = "module_pre_assessment.html"

	

	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		teacher_profile = None
		print("teacher does not exist")

	try:

		student_profile = StudentProfile.objects.get(user= request.user)
		print("Student exists")

	except StudentProfile.DoesNotExist:
		print("Student does not exist")
		student_profile = None


	topic = Topic.objects.get(slug = topic_slug)
	instance = get_object_or_404(PrePostQuestion, id=id)

	next_lesson_button_clicked 			= 			request.POST.get('nextLessonButton')
	question_submission_button_clicked   =    request.POST.get('questionSubmitButton')


	try:
		user_answer_instance = UserPreAssessmentAnswer.objects.get(user=request.user, question=instance)
		updated_q = True
	except UserPreAssessmentAnswer.DoesNotExist:
		user_answer_instance = UserPreAssessmentAnswer()
		updated_q = False
	except UserPreAssessmentAnswer.MultipleObjectsReturned:
		user_answer_instance = UserPreAssessmentAnswer.objects.filter(user=request.user, question=instance)[0]
		updated_q = True
	except:
		user_answer_instance = UserPreAssessmentAnswer()
		updated_q = False



	if next_lesson_button_clicked:
		print("Inside next_lesson_button_clicked button clicked")
		if user_answer_instance is not None:
			print("user answer instance is not None")
			question_id = request.POST.get("question_id")
			answer_id = request.POST.get("answer_id")

			if question_id is not None and answer_id is not None:
				question = instance
				answer = PrePostAnswer.objects.get(id = answer_id)

				
				user_answer_instance.user = request.user
				user_answer_instance.topic = topic
				user_answer_instance.question = question
				user_answer_instance.my_answer = answer
				user_answer_instance.answer_given = True
				user_answer_instance.correct_answer = True
				user_answer_instance.attempts_used = user_answer_instance.attempts_used + 1
				user_answer_instance.save()
				

				next_q = PrePostQuestion.objects.get_unanswered(request.user,topic).order_by("?")
				print ("The qs from form 1 is", next_q)
				if next_q.count() > 0:

					next_q_instance = next_q.first()
					
					return redirect("module_pre_assessment", course_slug=course_slug,topic_slug=topic_slug,id=next_q_instance.id)
					
				else:
					user_pre_assessment_status = UserPreAssessmentStatus()
					user_pre_assessment_status.user = request.user
					user_pre_assessment_status.topic = topic
					user_pre_assessment_status.completed = True
					user_pre_assessment_status.save()

					return redirect("module_lesson_list", course_slug=course_slug,topic_slug=topic_slug)



	# 		messages.error(request,"You submitted a wrong answer. You got one more chance,lets do it!")



	context = {
		"topic":topic,
		"teacher_profile":teacher_profile,
		"instance":instance,
		"user_answer_instance":user_answer_instance,
	}

	return render(request,template,context)



def module_post_assessment(request,id,course_slug,topic_slug):

	template = "module_post_assessment.html"

	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")

	try:

		student_profile = StudentProfile.objects.get(user= request.user)
		print("Student exists")

	except StudentProfile.DoesNotExist:
		print("Student does not exist")


	topic = Topic.objects.get(slug = topic_slug)
	instance = get_object_or_404(PrePostQuestion, id=id)

	next_lesson_button_clicked 			= 			request.POST.get('nextLessonButton')
	question_submission_button_clicked   =    request.POST.get('questionSubmitButton')


	try:
		user_answer_instance = UserPostAssessmentAnswer.objects.get(user=request.user, question=instance)
		updated_q = True
	except UserPreAssessmentAnswer.DoesNotExist:
		user_answer_instance = UserPostAssessmentAnswer()
		updated_q = False
	except UserPreAssessmentAnswer.MultipleObjectsReturned:
		user_answer_instance = UserPostAssessmentAnswer.objects.filter(user=request.user, question=instance)[0]
		updated_q = True
	except:
		user_answer_instance = UserPostAssessmentAnswer()
		updated_q = False



	if next_lesson_button_clicked:
		print("Inside next_lesson_button_clicked button clicked")
		if user_answer_instance is not None:
			print("user answer instance is not None")
			question_id = request.POST.get("question_id")
			answer_id = request.POST.get("answer_id")

			if question_id is not None and answer_id is not None:
				question = instance
				answer = PrePostAnswer.objects.get(id = answer_id)

				
				user_answer_instance.user = request.user
				user_answer_instance.question = question
				user_answer_instance.my_answer = answer
				user_answer_instance.answer_given = True
				user_answer_instance.correct_answer = True
				user_answer_instance.attempts_used = user_answer_instance.attempts_used + 1
				user_answer_instance.save()
				

				next_q = PrePostQuestion.objects.get_post_unanswered(request.user,topic.slug).order_by("?")
				print ("The qs from form 1 is", next_q)
				if next_q.count() > 0:

					next_q_instance = next_q.first()
					
					return redirect("module_post_assessment", course_slug=course_slug,topic_slug=topic_slug,id=next_q_instance.id)
					
				else:
					user_post_assessment_status = UserPostAssessmentStatus()
					user_post_assessment_status.user = request.user
					user_post_assessment_status.topic = topic
					user_post_assessment_status.completed = True
					user_post_assessment_status.save()

					return redirect("module_lesson_list", course_slug=course_slug,topic_slug=topic_slug)




	context = {
		"topic":topic,
		"teacher_profile":teacher_profile,
		"instance":instance,
		"user_answer_instance":user_answer_instance,
	}

	return render(request,template,context)


def module_lesson(request,course_slug,topic_slug,lesson_slug,lessoncontent_slug):
	template = "module_lesson.html"
	teacher_profile = None
	student_profile = None
	user_answer_instance = None
	user_text_answer = None
	free_text_answer = None
	print("User Answer instance is initially", user_answer_instance)
	print("User Text Answer instance is initially", user_text_answer)
	print("Free Text Answer instance is initially", free_text_answer)




	print("REQUEST SESSION ITEMS", request.session.items())


	topic = Topic.objects.get(slug = topic_slug)

	nlp_model_form =  NLPModelForm(request.POST or None)


	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")

	try:

		student_profile = StudentProfile.objects.get(user= request.user)
		print("Student exists")

	except StudentProfile.DoesNotExist:
		print("Student does not exist")


	lessons = Lesson.objects.get(slug = lesson_slug)
	
	lessons_content = lessons.lessoncontent_set.all()
	total_question_count = lessons_content.exclude(question__isnull=True).count()

	if lessons_content.count() > 0:
		current_instance = lessons_content.get(slug=lessoncontent_slug)
		lessons_content = lessons_content.filter(id__gt = current_instance.id)
		
		next_instance = lessons_content.first()
	else:
		current_instance = lessons_content.first()

	

	if not lesson_slug in request.session:
		request.session[lesson_slug] = 1
	else:
		if request.session[lesson_slug] > total_question_count:
			request.session[lesson_slug] = 1

		

	# current_uncompleted_instance = uncompleted_lessons.first()

	# if uncompleted_lessons.count() == 1:
	
	print("Current is",current_instance)
	print("Next is",next_instance)

	next_lesson_button_clicked = 			request.POST.get('nextLessonButton')
	question_submission_button_clicked =    request.POST.get('questionSubmitButton')


	if request.user.teacher_student == "Student" and current_instance.question and current_instance.question.question_type == "multiple_choice":
		user_text_answer = None
		free_text_answer = None
		try:
			user_answer_instance = UserAnswer.objects.get(user=request.user, question=current_instance.question)

		except UserAnswer.DoesNotExist:
			user_answer_instance = UserAnswer()

		except UserAnswer.MultipleObjectsReturned:
			user_answer_instance = UserAnswer.objects.filter(user=request.user, question=current_instance.question)[0]

		except:
			user_answer_instance = UserAnswer()

	elif request.user.teacher_student == "Student" and current_instance.question and current_instance.question.question_type == "text_input":
		user_answer_instance = None
		try:
			user_text_answer = UserTextAnswer.objects.get(user=request.user, question=current_instance.question)
			
		except UserTextAnswer.DoesNotExist:
			user_text_answer = UserTextAnswer()
			
		except UserTextAnswer.MultipleObjectsReturned:
			user_text_answer = UserTextAnswer.objects.filter(user=request.user, question=current_instance.question)[0]
			
		except:
			user_text_answer = UserTextAnswer()
			



		try:
			free_text_answer = FreeTextAnswer.objects.get(question_id=current_instance.question.id)
		except FreeTextAnswer.DoesNotExist:
			free_text_answer = FreeTextAnswer()
		except FreeTextAnswer.MultipleObjectsReturned:
			free_text_answer = FreeTextAnswer.objects.filter(question_id=current_instance.question.id)[0]
		except:
			free_text_answer = FreeTextAnswer()

			
	elif request.user.teacher_student == "Teacher" and current_instance.question:
		user_answer_instance = None
		user_text_answer = None
		free_text_answer = None


	user_free_text_form = UserTextFreeForm(request.POST or None)	




	if question_submission_button_clicked:
		if user_answer_instance is not None:
			question_id = request.POST.get("question_id")
			answer_id = request.POST.get("answer_id")


			if not user_answer_instance.attempts_used == 2 and not user_answer_instance.answer_given:
				print("User answer given status is " , user_answer_instance.answer_given)
				if question_id is not None and answer_id is not None:
					question = Question.objects.get(id = question_id)
					answer = Answer.objects.get(id = answer_id)

					if question.correct_answer == answer.text or question.correct_answer == "All":
						user_answer_instance.user = request.user
						user_answer_instance.question = question
						user_answer_instance.my_answer = answer
						user_answer_instance.answer_given = True
						user_answer_instance.correct_answer = True
						user_answer_instance.attempts_used = user_answer_instance.attempts_used + 1
						user_answer_instance.save()
						messages.success(request,"Correct Answer")
					else:
						user_answer_instance.user = request.user
						user_answer_instance.question = question
						user_answer_instance.my_answer = answer
						user_answer_instance.attempts_used = user_answer_instance.attempts_used + 1
						if user_answer_instance.attempts_used == 2:

							user_answer_instance.answer_given = True
							user_answer_instance.correct_answer = False
						else:
							user_answer_instance.answer_given = False

						user_answer_instance.save()
						print("Wrong Answer")
						messages.error(request,"Wrong Answer")
			elif user_answer_instance.attempts_used == 2:
				messages.error(request,"Total Number Of Attemptes Used. (2/2)")
			elif user_answer_instance.answer_given:
				messages.error(request,"You have already answered.")

		elif user_text_answer is not None and free_text_answer is not None:

			if user_free_text_form.is_valid():

				print("Inside User Free Text Form")
				question_id = user_free_text_form.cleaned_data.get('question_id')
				user_text_input = user_free_text_form.cleaned_data.get('my_answer')
				print("The question id is ", question_id, "and the answer is ", user_text_input)
				question_instance = Question.objects.get(id=question_id)
				
				
				free_text_answer.question = question_instance
				free_text_answer.text = user_text_input
				free_text_answer.save()

					
				user_text_answer.user = request.user
				user_text_answer.question = question_instance
				user_text_answer.my_answer = free_text_answer
				user_text_answer.answer_given = True
				user_text_answer.save()
				messages.success(request,"Answer Submitted Successfully")






		print("Question Submission Button Clicked")


	if next_lesson_button_clicked == "nextLessonButton" and lessons_content.count() > 0:

		if request.user.teacher_student == "Student" and current_instance.question and current_instance.question.question_type  == "multiple_choice":

			if user_answer_instance.attempts_used == 2 or user_answer_instance.answer_given == True:

				request.session[lesson_slug] = request.session[lesson_slug] + 1
				return redirect("module_lesson", course_slug=course_slug,topic_slug=topic_slug,lesson_slug=lesson_slug,lessoncontent_slug=next_instance.slug)
			else:
				messages.error(request,"Answer the question to move forward")


		elif request.user.teacher_student == "Student" and current_instance.question and current_instance.question.question_type  == "text_input":
			if user_text_answer.answer_given == True:
				request.session[lesson_slug] = request.session[lesson_slug] + 1
				return redirect("module_lesson", course_slug=course_slug,topic_slug=topic_slug,lesson_slug=lesson_slug,lessoncontent_slug=next_instance.slug)
			else:
				messages.error(request,"Answer the question to move forward")


		

		elif request.user.teacher_student == "Student" and not current_instance.question:
			return redirect("module_lesson", course_slug=course_slug,topic_slug=topic_slug,lesson_slug=lesson_slug,lessoncontent_slug=next_instance.slug)

		elif request.user.teacher_student == "Teacher":
			if current_instance.question:
				request.session[lesson_slug] = request.session[lesson_slug] + 1
			return redirect("module_lesson", course_slug=course_slug,topic_slug=topic_slug,lesson_slug=lesson_slug,lessoncontent_slug=next_instance.slug)

		

	elif next_lesson_button_clicked == "nextLessonButton" and lessons_content.count() == 0:

		request.session[lesson_slug] = 1
		if request.user.teacher_student == "Student":
			print("Student Profile is",student_profile)
			user_lesson_completion_instance = UserLessonCompletionStatus()
			user_lesson_completion_instance.student_profile = student_profile
			user_lesson_completion_instance.lesson = lessons
			user_lesson_completion_instance.lesson_completed = True
			user_lesson_completion_instance.topic = topic
			user_lesson_completion_instance.course = Course.objects.get(slug = course_slug)

			user_lesson_completion_instance.save()

		return redirect("module_lesson_list", course_slug=course_slug,topic_slug=topic_slug)






	print("User Answer instance after", user_answer_instance)
	print("User Text Answer instance after", user_text_answer)
	print("Free Text Answer instance after", free_text_answer)

	context = {
		"topic":topic,
		"teacher_profile":teacher_profile,
		"student_profile":student_profile,
		"current_instance":current_instance,
		"user_answer_instance":user_answer_instance,
		"user_text_answer": user_text_answer, 
		"free_text_answer": free_text_answer,
		"user_free_text_form":user_free_text_form,
		"nlp_model_form":nlp_model_form,
		"current_question_count": request.session[lesson_slug],
		"total_question_count":total_question_count,
	}
	return render(request,template,context)



#Teacher -- Students Navbar Item


def products_list(request):
	template = "products_list.html"
	teacher_profile = None
	student_profile = None
	products = Products.objects.all()
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	if teacher_profile and not teacher_profile.verified:
		return redirect("account_profile")


	teacher_classes = teacher_profile.teacherclass_set.all()
	

	teacher_class_count = teacher_classes.values_list("course__product__name").annotate(Count('name'))
	print(teacher_class_count)
	context = {
		"products":products,
		"teacher_profile":teacher_profile,
		"teacher_class_count":teacher_class_count,
	}

	return render(request,template,context)



def product_module_list(request,slug):
	template = "product_module_list.html"
	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")

	courses = Course.objects.filter(product__slug = slug)
	print("Courses are ", courses)


	teacher_classes = teacher_profile.teacherclass_set.all()
	

	teacher_class_count = teacher_classes.values_list("course__name").annotate(Count('name'))
	print("teacher class count is", teacher_class_count)

	products = Products.objects.all()
	context = {
		"products":products,
		"teacher_profile":teacher_profile,
		"courses":courses,
		"teacher_class_count":teacher_class_count,
	}

	return render(request,template,context)

def product_module_class_list(request,product_slug,course_slug):
	template = "product_module_class_list.html"
	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	course = Course.objects.get(slug = course_slug)
	products = Products.objects.all()
	# for single_class in teacher_classes:
	# 	students = single_class.studentprofile_set.all()
	# 	for student in students:
	# 		student_answers = student.user.useranswer_set.all()
	# 		qs = student_answers.values('my_points').annotate(Sum('my_points'))
	# 		count = qs[0]['my_points__sum']

	teacher_class = TeacherClass.objects.filter(course__slug = course_slug,teacher = teacher_profile)
	print("Teacher Class is",teacher_class)


	context = {
		"products":products,
		"teacher_profile":teacher_profile,
		"teacher_class":teacher_class,
	}

	return render(request,template,context)

def class_students_list(request,slug):
	template = "class_students_list.html"
	teacher_class = TeacherClass.objects.get(slug = slug)
	products = Products.objects.all()

	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")

	context = {
		"teacher_class":teacher_class,
		"products":products,
		"teacher_profile":teacher_profile,

	}
	return render(request,template,context)


#  ---------------------------- DASHBOARD FUNCTIONS START HERE -------------------------------------

def dashboard(request):

	template = "dashboard.html"

	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	if teacher_profile and not teacher_profile.verified:
		return redirect("account_profile")
	elif student_profile and student_profile.course.all().count() == 0:
		return redirect("account_profile")

	mod_completion_list = None
	if student_profile is None:
		mod_completion_list = []
		teacher_students = teacher_profile.studentprofile_set.all()
		students_emails = teacher_students.values_list('user__email',flat=True)
		qs_res =UserLessonCompletionStatus.objects.filter(student_profile__user__email__in = students_emails,
														topic_completed=True).values('student_profile__user__email',
																						'course__name','topic__name').distinct()

		for course in teacher_profile.course.all():
			count = 0
			for data in qs_res:
				if data['course__name'] == course.name:
					count = count + 1
			
			mod_completion_list.append(count)





	student_dict = {}
	try:
		teacher_classes = teacher_profile.teacherclass_set.all()
	except:
		teacher_classes = []
	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email

	context = {
	"teacher_profile":teacher_profile,
	"student_profile":student_profile,
	"mod_completion_list":mod_completion_list,
	"student_dict":student_dict,
	}


	return render(request,template,context)


def dashboard_topic_list(request,slug):

	template = "dashboard_topic_list.html"
	teacher_profile = None
	student_profile = None

	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	course = Course.objects.get(slug = slug)


	topics = Topic.objects.filter(course__slug = slug)
	student_topic_completion_list = []
	for topic in topics:
		count = 0
		try:
			teacher_students  = teacher_profile.studentprofile_set.all()
		except:
			teacher_students = []
		for student in teacher_students:

			student_count = UserLessonCompletionStatus.objects.filter(student_profile = student,topic = topic,topic_completed=True).count()
			if student_count > 1:
				count = count + 1
			print("Student is", student, "count is student_count")
		student_topic_completion_list.append(count)



	students_topic_progress = {}
	if student_profile is None:
		for topic in topics:
			inner_dict = {}
			for students in teacher_profile.studentprofile_set.all():
				try:
					user_status = UserLessonCompletionStatus.objects.filter(student_profile = students,topic__slug = topic.slug).first()
					print("User is",students.user.email, "and completion status is", user_status.lesson_completed)
					if user_status.topic_completed:
						inner_dict[students.user.email] = {"topic_name":topic.name,"completed":"1"}
					else:
						inner_dict[students.user.email] = {"topic_name":topic.name,"completed":"0"}
				except:
					inner_dict[students.user.email] =     {"topic_name":topic.name,"completed":"0"}

			students_topic_progress[topic.name] = inner_dict



	topic_avg_list = []
	for topic in topics:

		lessons = Lesson.objects.filter(topic__slug = topic.slug)
		
		lesson_score_percentage = []
		for lesson in lessons:
			try:
				teacher_students_set = teacher_profile.studentprofile_set.all()
				students = teacher_students_set.values_list('user__email',flat=True)

			except:
				teacher_students_set = []
				students = []
			
			print("STudents are",students)

			answers = UserAnswer.objects.filter(user__email__in = students,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)

			questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
			if answers.count() == 0 and questions.count() == 0:
				lesson_score_percentage.append(0)
			else:
				lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))


		topic_average = sum(lesson_score_percentage)/lessons.count()
		topic_avg_list.append(topic_average)
	current_student = None
	if student_profile is not None:
		current_student = StudentProfile.objects.get(user__email = request.user.email)
		topic_avg_list = []
		for topic in topics:

			lessons = Lesson.objects.filter(topic__slug = topic.slug)
			
			lesson_score_percentage = []
			for lesson in lessons:

				answers = UserAnswer.objects.filter(user__email= request.user.email,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)

				questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
				if answers.count() == 0 and questions.count() == 0:
					lesson_score_percentage.append(0)
				else:
					lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))


			topic_average = sum(lesson_score_percentage)/lessons.count()
			topic_avg_list.append(topic_average)




	student_dict = {}
	try:
		teacher_classes = teacher_profile.teacherclass_set.all()
	except:
		teacher_classes = []
	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email




	context = {
		"course":course,
		"teacher_profile":teacher_profile,
		"student_profile":student_profile,
		"student_topic_completion_list":student_topic_completion_list,
		"students_topic_progress":students_topic_progress,
		"topic_avg_list":topic_avg_list,
		"student_dict":student_dict,
		"current_student":current_student,
	}


	return render(request,template,context)




def dashboard_lesson_list(request,course_slug,topic_slug):
	template = "dashboard_lesson_list.html"
	teacher_profile = None
	student_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	topic = Topic.objects.get(slug = topic_slug)
	question_count_list = get_lesson_question_count(topic)
	print("Question count list is", question_count_list)


	lessons = topic.lesson_set.all()
	completed_student_lessons_list = []
	if student_profile is None:
		for lesson in lessons:
			completed_lessons = UserLessonCompletionStatus.objects.filter(lesson__slug = lesson.slug,lesson_completed = True)



			
			# if completed_lessons.student_profile in teacher_profile.studentprofile_set.all():
			teacher_student_ids = teacher_profile.studentprofile_set.all().values_list('user__id',flat=True)
			completed_lesson_student_ids = completed_lessons.values_list('student_profile__user__id',flat=True)


			#previously used this -- which did not account for the teacher, gave the wrong count
			# completed_student_lessons_list.append(completed_lessons.count())
			#new piece of line
			completed_student_lessons_list.append(completed_lessons.count() -  len(set(completed_lesson_student_ids) - set(teacher_student_ids)) )
















	students_lesson_progress = {}
	if student_profile is None:
		for lesson in lessons:
			inner_dict = {}
			for students in teacher_profile.studentprofile_set.all():
				try:
					user_status = UserLessonCompletionStatus.objects.get(student_profile = students,lesson__slug = lesson.slug)
					print("User is",students.user.email, "and completion status is", user_status.lesson_completed)
					if user_status.lesson_completed:
						inner_dict[students.user.email] = {"lesson_name":lesson.name,"completed":"1"}
					else:
						inner_dict[students.user.email] = {"lesson_name":lesson.name,"completed":"0"}
				except:
					inner_dict[students.user.email] =     {"lesson_name":lesson.name,"completed":"0"}

			students_lesson_progress[lesson.name] = inner_dict
				
	print("Student lesson progress dict", students_lesson_progress)



	correct_answer_count = []
	total_question_count = []
	lesson_score_percentage = []
	for lesson in lessons:
		try:
			teacher_students_set = teacher_profile.studentprofile_set.all()
			students = teacher_students_set.values_list('user__email',flat=True)
			print("STudents are",students)
		except:
			teacher_students_set = []
			students = []	
		answers = UserAnswer.objects.filter(user__email__in = students,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
		correct_answer_count.append(answers.count())
		questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
		total_question_count.append(questions.count())
		print("Answer count is",answers.count(), "questions.count() is" ,questions.count())
		if answers.count() == 0 and questions.count() == 0:
			lesson_score_percentage.append(0)
		else:
			lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))


	if student_profile is not None:

		correct_answer_count = []
		total_question_count = []
		lesson_score_percentage = []
		for lesson in lessons:
		
			answers = UserAnswer.objects.filter(user__email = request.user.email,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
			correct_answer_count.append(answers.count())
			questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
			total_question_count.append(questions.count())
			print("Answer count is",answers.count(), "questions.count() is" ,questions.count())
			if answers.count() == 0 and questions.count() == 0:
				lesson_score_percentage.append(0)
			else:
				lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))


		print("answer count",correct_answer_count)
		print("total question count", total_question_count)

	student_dict = {}
	try:
		teacher_classes = teacher_profile.teacherclass_set.all()
	except:
		teacher_classes = []

	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email

	current_student = None
	if student_profile is not None:	
		current_student = StudentProfile.objects.get(user__email = request.user.email)
	
	lessons = Lesson.objects.filter(topic__slug = topic_slug)
	# correct_answer_count = []
	# total_question_count = []
	# lesson_score_percentage = []
	# for lesson in lessons:
	# 	answers = UserAnswer.objects.filter(question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
	# 	correct_answer_count.append(answers.count())
	# 	questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
	# 	total_question_count.append(questions.count())
	# 	print("Answer count is",answers.count(), "questions.count() is" ,questions.count())
	# 	if answers.count() == 0 and questions.count() == 0:
	# 		lesson_score_percentage.append(0)
	# 	else:
	# 		lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))








	#needed
	lesson_completion_list = []
	for lesson in lessons:
		try:
			lesson_status = UserLessonCompletionStatus.objects.get(lesson__slug = lesson.slug, topic__slug = topic_slug,student_profile__user__email = request.user.email)
			if lesson_status.lesson_completed:
				lesson_completion_list.append("1")
			else:
				lesson_completion_list.append("0")
		except:
			lesson_completion_list.append("0")
	print("Lesson completion list is",lesson_completion_list)


	context = {
		"topic":topic,
		"teacher_profile":teacher_profile,
		"student_profile":student_profile,
		"question_count_list":question_count_list,
		"completed_student_lessons_list":completed_student_lessons_list,
		"students_lesson_progress":students_lesson_progress,
		"lesson_score_percentage":lesson_score_percentage,
		"student_dict":student_dict,
		"lesson_completion_list":lesson_completion_list,
		"correct_answer_count":	correct_answer_count, 
		"total_question_count": total_question_count 
	}
	return render(request,template,context)



def dashboard_single_student_stats(request,slug):
	template = "dashboard_single_student_stats.html"
	teacher_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")

	current_student = StudentProfile.objects.get(user__username = slug)
	student_dict = {}
	teacher_classes = teacher_profile.teacherclass_set.all()
	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email

	



	context = {
		"student_dict":student_dict,
		"current_student":current_student,
		"teacher_profile":teacher_profile,

	}

	return render(request,template,context)


def dashboard_single_student_topic_list(request,user_slug, course_slug):
	template = "dashboard_single_student_topic_list.html"

	teacher_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")

	current_student = StudentProfile.objects.get(user__username = user_slug)
	student_dict = {}
	teacher_classes = teacher_profile.teacherclass_set.all()
	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email

	current_course = Course.objects.get(slug = course_slug)
	topics = Topic.objects.filter(course__slug = course_slug)

	# topic_avg_list = []
	# for topic in topics:

	# 	lessons = Lesson.objects.filter(topic__slug = topic.slug)
		
	# 	lesson_score_percentage = []
	# 	for lesson in lessons:
	# 		answers = UserAnswer.objects.filter(question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
	# 		questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
	# 		if answers.count() == 0 and questions.count() == 0:
	# 			lesson_score_percentage.append(0)
	# 		else:

	# 			lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))
	# 	topic_average = sum(lesson_score_percentage)/lessons.count()
	# 	topic_avg_list.append(topic_average)


	topic_avg_list = []
	for topic in topics:

		lessons = Lesson.objects.filter(topic__slug = topic.slug)
		
		lesson_score_percentage = []
		for lesson in lessons:
			teacher_students_set = teacher_profile.studentprofile_set.all()
			students = teacher_students_set.values_list('user__email',flat=True)
			print("STudents are",students)

			answers = UserAnswer.objects.filter(user__email__in = students,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)

			questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
			if answers.count() == 0 and questions.count() == 0:
				lesson_score_percentage.append(0)
			else:
				lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))


		topic_average = sum(lesson_score_percentage)/lessons.count()
		topic_avg_list.append(topic_average)






	context = {
		"teacher_profile":teacher_profile,
		"student_dict":student_dict,
		"current_student":current_student,
		"current_course":current_course,
		"topics":topics,
		"topic_avg_list":topic_avg_list,
	}

	return render(request,template,context)

def dashboard_single_student_lessons_list(request,user_slug, course_slug,topic_slug):
	template = "dashboard_single_student_lessons_list.html"

	teacher_profile = None
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")

	current_student = StudentProfile.objects.get(user__username = user_slug)
	student_dict = {}
	teacher_classes = teacher_profile.teacherclass_set.all()
	for single_class in teacher_classes:
		students = single_class.studentprofile_set.all()
		for student in students:
			if student.user.first_name and student.user.last_name  :

				student_dict[student.user.username] = student.user.first_name +" "+student.user.last_name
			else:
				student_dict[student.user.username] = student.user.email

	
	lessons = Lesson.objects.filter(topic__slug = topic_slug)
	# correct_answer_count = []
	# total_question_count = []
	# lesson_score_percentage = []
	# for lesson in lessons:
	# 	answers = UserAnswer.objects.filter(question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
	# 	correct_answer_count.append(answers.count())
	# 	questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
	# 	total_question_count.append(questions.count())
	# 	print("Answer count is",answers.count(), "questions.count() is" ,questions.count())
	# 	if answers.count() == 0 and questions.count() == 0:
	# 		lesson_score_percentage.append(0)
	# 	else:
	# 		lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))



	correct_answer_count = []
	total_question_count = []
	lesson_score_percentage = []
	for lesson in lessons:
		teacher_students_set = teacher_profile.studentprofile_set.all()
		students = teacher_students_set.values_list('user__email',flat=True)
		print("STudents are",students)			
		answers = UserAnswer.objects.filter(user__email__in = students,question__lessoncontent__lesson__slug = lesson.slug,correct_answer=True)
		correct_answer_count.append(answers.count())
		questions = Question.objects.filter(lessoncontent__lesson__slug = lesson.slug,question_type="multiple_choice")
		total_question_count.append(questions.count())
		print("Answer count is",answers.count(), "questions.count() is" ,questions.count())
		if answers.count() == 0 and questions.count() == 0:
			lesson_score_percentage.append(0)
		else:
			lesson_score_percentage.append(math.ceil((answers.count()/questions.count()) * 100 ))



	print("answer count",correct_answer_count)
	print("total question count", total_question_count)


	lesson_completion_list = []
	for lesson in lessons:
		try:
			lesson_status = UserLessonCompletionStatus.objects.get(lesson__slug = lesson.slug, topic__slug = topic_slug,student_profile__user__username = user_slug)
			if lesson_status.lesson_completed:
				lesson_completion_list.append("1")
			else:
				lesson_completion_list.append("0")
		except:
			lesson_completion_list.append("0")


	context = {
		"teacher_profile":teacher_profile,
		"student_dict":student_dict,
		"current_student":current_student,
		"lessons":lessons,
		"correct_answer_count":correct_answer_count,
		"total_question_count":total_question_count,
		"lesson_completion_list":lesson_completion_list,
		"lesson_score_percentage":lesson_score_percentage,

	
	}

	return render(request,template,context)






def lesson_vocabulary(request,slug):
	template = "lesson_vocabulary.html"
	teacher_profile = None
	student_profile = None

	vocabulary = Vocabulary.objects.filter(lesson__slug = slug)

	lesson = Lesson.objects.get(slug = slug)




	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	if teacher_profile and not teacher_profile.verified:
		return redirect("account_profile")
	elif student_profile and student_profile.course.all().count() == 0:
		return redirect("account_profile")

	context = {
		"teacher_profile":teacher_profile,
		"student_profile":student_profile,
		"vocabulary":vocabulary,
		"lesson":lesson,
	}
	return render(request,template,context)








from rest_framework.response import Response
import glob as glob
import json
from django.http import JsonResponse, HttpResponse

import os
from django.conf import settings
import gensim as gensim


import spacy

import numpy as np
from sklearn.decomposition import PCA

#Load these models
nlp = spacy.load("en_core_web_lg")

MEDIA_ROOT = os.path.join(settings.BASE_DIR, "nlp_models")
models_list = sorted(glob.glob(MEDIA_ROOT+'/*.bin'))
model_name = models_list[-1]
model = gensim.models.KeyedVectors.load_word2vec_format(model_name, binary=True)


# def similar_word_rankings(a, n=10):
#     sims = model.most_similar(a, topn = n)
#     dic = dict(sims)
#     similar_words = {k: v for k, v in dic.items() if not k.startswith("wiki")}
#     similar_words = {k: similar_words[k] for k in list(similar_words)[:n]}
#     return similar_words



def word_vectorizer(words):

    word_vecs = []

    for w in words:
        v = model.wv[w]
        word_vecs.append(v)

    vec = np.sum(word_vecs, axis = 0)

    return vec



def similar_word_rankings(a, n):
    sims = model.most_similar(a, topn = n)
    dic = dict(sims)
    similar_words = {k: v for k, v in dic.items() if not k.startswith("wiki")}
    similar_words = {k: similar_words[k] for k in list(similar_words)[:n]}
    return similar_words


def PCA_reduction(word_vec, dims = 2):

    pca = PCA(n_components=2)
    result = pca.fit_transform(word_vec)

    return result


def sim_matrix(sims):

    vecs = np.zeros((len(sims), 300))
    i = 0
    for k, v in sims.items():
        vec = word_vectorizer([k])
        vecs[i, :] = vec
        i = i+1
    return vecs


def ajax_similar_word_rankings(request):
	a = request.GET.get("userWord")
	n = 10
	similar_words = similar_word_rankings(a,10)
	# sims = model.most_similar(a, topn = n)
	# dic = dict(sims)
	# similar_words = {k: v for k, v in dic.items() if not k.startswith("wiki")}
	# similar_words = {k: similar_words[k] for k in list(similar_words)[:n]}
	 
	data= {
			"Success":similar_words
		}
	return JsonResponse(data)



def ajax_ner_model(request):

	input_string = request.GET.get("userWord")
	output = {}
	doc = nlp(input_string)
	word_forms = ['PERSON', 'ORG', 'PRODUCT']
	for ent in doc.ents:
	    if ent.label_ in word_forms:
	        output[ent.text] = ent.label_

	data= {
		"Success":output
	}
	return JsonResponse(data)

def ajax_pos_model(request):
    
	input_string = request.GET.get("userWord")
	output = {}
	doc = nlp(input_string)
	verb_forms = ['PROPN', 'VERB', 'ADV', 'NOUN']
	for token in doc:
	    if token.pos_ in verb_forms:
	        output[token.text] = token.pos_
	data= {
		"Success":output
	}
	return JsonResponse(data)






def ajax_sim_to_words(request):
	userWord = request.GET.get("userWord")
	wordlist = userWord.split(',')
	word1 = wordlist[0]
	word2 = wordlist[1]
	wordsim = model.similarity(word1, word2)
	output = {}
	output[str(wordlist)] = str(wordsim)
	data= {
		"Success":output,
	}
	return JsonResponse(data)



def ajax_word_addition(request):
	string = request.GET.get("userWord")
	output = {}
	words = string.split(',')
	result = model[words[0]] + model[words[1]] - model[words[2]]
	result_word = model.most_similar(positive=[result], topn=5)
	for w in result_word:
		output[str(w[0])] = str(w[1]) 
	data= {
		"Success":output,
	}
	return JsonResponse(data)





def ajax_word_to_plot(request):
	words = request.GET.get("userWord")
	vocab = []

	mats = []
	words = words.split(',')
	for w in words:
	    sims = similar_word_rankings(w, 10)
	    for k, v in sims.items():
	        vocab.append(k)
	    sm = sim_matrix(sims)
	    mats.append(sm)

	final_mat = np.vstack((m for m in mats))
	result = PCA_reduction(final_mat)
	output = {}

	for w in range(len(vocab)):

	    output[vocab[w]] = [result[w, 0], result[w, 1]]

	data= {
		"Success":output,
	}
	return JsonResponse(data)

