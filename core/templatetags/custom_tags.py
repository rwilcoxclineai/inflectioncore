#this should be at the top of your custom template tags file
from django.template import Library, Node, TemplateSyntaxError
register = Library()


@register.filter
def get_index(l, i):
    return l[i]


@register.filter
def get_teacher_class_count(l,n):
	print("The l is",l)
	print("the n is",n)
	
	for s in l:
		if s[0] == n:
			return s[1]

@register.simple_tag(takes_context=True)
def student_lesson_progress_dict(context , d, inner_dict_key):
	print("Inside custom template tags")
	current_dict = context["students_lesson_progress"]
	inner_dict = current_dict.get(d)
	return inner_dict[inner_dict_key]["completed"]


@register.simple_tag(takes_context=True)
def student_topic_progress_dict(context , d, inner_dict_key):
	print("Inside custom template tags")
	current_dict = context["students_topic_progress"]
	inner_dict = current_dict.get(d)
	return inner_dict[inner_dict_key]["completed"]