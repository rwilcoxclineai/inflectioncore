from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth import get_user_model
from .models import *
from django.contrib.auth.models import Group


from .forms import UserAdminCreationForm, UserAdminChangeForm


User = get_user_model()

class QuestionAdmin(admin.ModelAdmin):

    list_display = ('text', 'active', 'question_type','timestamp','updated')
    readonly_fields = ('timestamp','updated',)


admin.site.register(Question,QuestionAdmin)



class AnswerAdmin(admin.ModelAdmin):

    list_display = ('text', 'active', 'question','get_question_id','timestamp','updated')
    readonly_fields = ('timestamp','updated',)
    def get_question_id(self, obj):
        return obj.question.id

    get_question_id.short_description = 'Question ID'
    get_question_id.admin_order_field = 'question__id'


admin.site.register(Answer,AnswerAdmin)




class SchoolAdmin(admin.ModelAdmin):

    list_display = ('name', 'address', 'contact','district','school_uuid')
    readonly_fields = ('timestamp','updated','school_uuid')


admin.site.register(School,SchoolAdmin)


class UserAnswerAdmin(admin.ModelAdmin):

    list_display = ('user', 'my_answer', 'question')
    readonly_fields = ('timestamp',)


admin.site.register(UserAnswer,UserAnswerAdmin)

admin.site.register(FreeTextAnswer)
admin.site.register(UserTextAnswer)
admin.site.register(Course)

admin.site.register(Topic)
admin.site.register(SubTopic)
admin.site.register(SubTopicContent)

admin.site.register(TestFile)
admin.site.register(Vocabulary)
admin.site.register(Lesson)
admin.site.register(Class)
admin.site.register(Products)




admin.site.register(UserLessonCompletionStatus)

admin.site.register(UserPreAssessmentAnswer)
admin.site.register(UserPreAssessmentStatus)
admin.site.register(UserPostAssessmentAnswer)
admin.site.register(UserPostAssessmentStatus)


class LessonContentAdmin(admin.ModelAdmin):

    search_fields = ["slug"]
    class Meta:
        model = LessonContent

admin.site.register(LessonContent,LessonContentAdmin)


class PrePostAnswerInline(admin.TabularInline):
    model = PrePostAnswer
    extra = 0
    max_num = 10



class PrePostQuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'active', 'question_type','timestamp','updated')
    inlines = [
        PrePostAnswerInline,
    ]
    class Meta:
        model = PrePostQuestion


admin.site.register(PrePostQuestion,PrePostQuestionAdmin)
admin.site.register(PrePostAnswer)

class TeacherProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'student_auth_uuid', 'school')
    readonly_fields = ('timestamp','updated','student_auth_uuid')

admin.site.register(TeacherProfile,TeacherProfileAdmin)



class StudentProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'school')
    readonly_fields = ('timestamp','updated',)

admin.site.register(StudentProfile,StudentProfileAdmin)




class TeacherClassAdmin(admin.ModelAdmin):

    list_display = ('course', 'name','slug','teacher','class_uuid')
    readonly_fields = ('class_uuid',)
admin.site.register(TeacherClass,TeacherClassAdmin)


class TeacherCourseUUIDAdmin(admin.ModelAdmin):

    list_display = ('teacher', 'course','course_uuid')
    readonly_fields = ('timestamp','updated','course_uuid')


admin.site.register(TeacherCourseUUID,TeacherCourseUUIDAdmin)







class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', )
    list_filter = ( 'is_active',)
    fieldsets = (
        (None, {'fields': ('first_name','last_name', 'email', 'password','username')}),
       # ('Full name', {'fields': ()}),
        ('Permissions', {'fields': ( 'is_active',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email', 'first_name','last_name')
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User,UserAdmin)
admin.site.unregister(Group)
