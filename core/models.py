# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
# Create your models here.
import uuid
from django.db.models import Q

from django.contrib.auth import get_user_model


from django.contrib.auth.models import AbstractUser,BaseUserManager

from django.db.models.signals import pre_save
from .utils import unique_slug_generator
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
User = settings.AUTH_USER_MODEL



Question_Types_Choices = (
    ('multiple_choice', ('multiple_choice')),
    ('text_input', ('text_input'))
)





class Products(models.Model):
	name = models.CharField(max_length=1000)
	slug = models.SlugField(blank=True,null=True,unique=True)

	def __str__(self):
		return self.name

	def get_slug_url(self):
		return f"/students/products/{self.slug}"

class QuestionManager(models.Manager):
	def get_unanswered(self, user,topic):
		q1 = Q(userpreassessmentanswer__user = user)
		# q2 = Q(userpreassessmentanswer__topic = topic)
		q3 = Q(assessment_type = 'post_assessment')
		qs = self.exclude(q1 | q3)
		print("The [pre qs is",qs)
		return qs

	def get_post_unanswered(self, user,topic_slug):
		q1 = Q(userpostassessmentanswer__user = user)
		# q2 = Q(userpostassessmentanswer__topic__slug = topic_slug)
		q3 = Q(assessment_type = 'pre_assessment')
		qs = self.exclude(q1 | q3)
		return qs






class School(models.Model):
	name = models.CharField(max_length = 500)
	address = models.CharField(max_length = 500)
	contact = models.CharField(max_length = 500)
	district = models.CharField(max_length = 500)
	school_uuid =  models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True)


	def __str__(self):
		return self.name



Assessment_Type_Choices = (
    ('pre_assessment', ('pre_assessment')),
    ('post_assessment', ('post_assessment'))
)



class Question(models.Model):
	text = models.TextField()
	active = models.BooleanField(default=True)
	question_type = models.CharField(max_length = 250, choices = Question_Types_Choices)
	correct_answer = models.TextField(blank=True,null=True)
	attempts_allowed = models.IntegerField(default = 2)
	pre_assessment = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True)


	objects = QuestionManager()


	def __str__(self):
		return self.text


class PrePostQuestion(models.Model):
	text = RichTextUploadingField()
	active = models.BooleanField(default=True)
	topic  = models.ForeignKey('Topic',on_delete=models.CASCADE)
	question_type = models.CharField(max_length = 250, choices = Question_Types_Choices)
	correct_answer = models.TextField(blank=True,null=True)
	attempts_allowed = models.IntegerField(default = 2)
	assessment_type = models.CharField(max_length = 250, choices = Assessment_Type_Choices)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True)


	objects = QuestionManager()


	def __str__(self):
		return self.text



class Answer(models.Model):
	question = models.ForeignKey(Question,on_delete=models.CASCADE)
	text = models.TextField(blank=True,null=True)
	active = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.text


class FreeTextAnswer(models.Model):
	question = models.ForeignKey(Question,on_delete=models.CASCADE)
	text = models.TextField(blank=True, null=True)
	active = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)

	def __unicode__(self):
		return self.text[:10]



class UserAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	question = models.ForeignKey(Question,on_delete=models.CASCADE)  
	my_answer = models.ForeignKey(Answer,on_delete=models.CASCADE,blank=True,null=True)
	my_points = models.IntegerField(default=-1)
	attempts_used = models.IntegerField(default=0)
	answer_given = models.BooleanField(default=False)
	correct_answer = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)


	def __unicode__(self):
		return self.my_answer.text[:10]



class PrePostAnswer(models.Model):
	question = models.ForeignKey(PrePostQuestion,on_delete=models.CASCADE)
	text = models.TextField(blank=True,null=True)
	active = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.text


class UserPreAssessmentAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE,blank=True,null=True)
	question = models.ForeignKey(PrePostQuestion,on_delete=models.CASCADE)  
	my_answer = models.ForeignKey(PrePostAnswer,on_delete=models.CASCADE,blank=True,null=True)
	my_points = models.IntegerField(default=-1)
	attempts_used = models.IntegerField(default=0)
	answer_given = models.BooleanField(default=False)
	correct_answer = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)


	def __unicode__(self):
		return self.my_answer.text[:10]


class UserPostAssessmentAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE,blank=True,null=True)
	question = models.ForeignKey(PrePostQuestion,on_delete=models.CASCADE)  
	my_answer = models.ForeignKey(PrePostAnswer,on_delete=models.CASCADE,blank=True,null=True)
	my_points = models.IntegerField(default=-1)
	attempts_used = models.IntegerField(default=0)
	answer_given = models.BooleanField(default=False)
	correct_answer = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)


	def __unicode__(self):
		return self.my_answer.text[:10]






class UserTextAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	question = models.ForeignKey(Question,on_delete=models.CASCADE)  
	my_answer = models.ForeignKey(FreeTextAnswer,on_delete=models.CASCADE)
	my_points = models.IntegerField(default=-1)
	answer_given = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __unicode__(self):
		return self.my_answer.text[:10]



def image_upload_to(instance, filename):
	title = instance.name

	return "course-images/%s/%s" %(title, filename)


class Class(models.Model):
	name = models.CharField(max_length=255)
	slug = models.SlugField()
	class_uuid = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)

	def __str__(self):
		return self.name

class TeacherClass(models.Model):
	course = models.ForeignKey('Course',on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	slug = models.SlugField()
	teacher = models.ForeignKey('TeacherProfile',on_delete=models.CASCADE,blank=True,null=True)
	class_uuid = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)

	def __str__(self):
		return self.name

	def get_class_student_list_url(self):
		return f"/{self.slug}/students"






class Course(models.Model):
	product = models.ForeignKey('Products',on_delete=models.CASCADE)
	name = models.CharField(max_length = 255)
	slug = models.SlugField(blank=True,null=True,unique=True)
	image   = models.ImageField(upload_to=image_upload_to, blank=True, null=True)
	course_uuid = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)
	updated = models.DateTimeField(auto_now=True)



	def __str__(self):
		return self.name

	def get_slug_url(self):

		return f"/course/{self.slug}"

	def get_dashboard_topic_url(self):
		return f"/dashboard/{self.slug}"


	def get_teacher_class_list_url(self):

		return f"/students/products/{self.slug}/classes"


	def get_absolute_url(self):

		return f"/accounts/profile/{self.course_uuid}"

	def get_absolute_url_student(self):

		return f"/accounts/profile/student/{self.course_uuid}"

	def get_subscribe_url(self):
		return f"{self.get_absolute_url()}/subscribe"

	def get_unsubscribe_url(self):
		return f"{self.get_absolute_url()}/unsubscribe"

	def get_question_absolute_url(self):
		return f"/questions/{self.id}"

	def get_student_product_class_url(self):
		return f"/students/products/{self.product.slug}/{self.slug}/classes"



class Topic(models.Model):
	course = models.ForeignKey('Course',on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	description = models.CharField(max_length=255)
	slug = models.SlugField(blank=True,null=True)


	def __str__(self):
		return self.name

	def get_slug_url(self):

		return f"/course/{self.course.slug}/{self.slug}"

	def get_pre_assessment_url(self):

		return f"/course/{self.course.slug}/{self.slug}/pre-assessment/{self.prepostquestion_set.all().filter(assessment_type = 'pre_assessment')[0].id}"

	def get_post_assessment_url(self):
		post_assessment_question = self.prepostquestion_set.all().filter(assessment_type = 'post_assessment')
		if post_assessment_question.count() > 0:

			return f"/course/{self.course.slug}/{self.slug}/post-assessment/{self.prepostquestion_set.all().filter(assessment_type = 'post_assessment')[0].id}"
		else:
			return f"/course/{self.course.slug}/{self.slug}"


	def get_dashboard_slug_url(self):

		return f"/dashboard/{self.course.slug}/{self.slug}"




class Lesson(models.Model):
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	slug = models.SlugField()

	class Meta:
		ordering = ['id']

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return f"/course/{self.topic.course.slug}/{self.topic.slug}/{self.slug}"




	def get_first_lessoncontent_obj(self):
		return self.lessoncontent_set.all()[0]

	def get_lesson_vocab_url(self):
		return f"/vocabulary/{self.slug}"


class LessonContent(models.Model):
	lesson = models.ForeignKey('Lesson',on_delete=models.CASCADE,blank=True,null=True)
	name = models.CharField(max_length=500)
	slug = models.SlugField(unique=True,blank=True,null=True)
	question = models.ForeignKey(Question,on_delete=models.CASCADE,blank=True,null=True)
	content = RichTextUploadingField()
	teacher_content = RichTextUploadingField(blank=True,null=True)
	uses_nlp_models = models.BooleanField(default=False)
	model_name = models.CharField(max_length = 255,blank=True,null=True)

	class Meta:
		ordering = ["id"]

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return f"/course/{self.lesson.topic.course.slug}/{self.lesson.topic.slug}/{self.lesson.slug}/{self.slug}"


		

class SubTopic(models.Model):
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	description = models.CharField(max_length = 255)

	def __str__(self):
		return self.name


class SubTopicContent(models.Model):
	sub_topic = models.ForeignKey('SubTopic',on_delete=models.CharField)
	content =  models.TextField()

	def __str__(self):
		return self.content[:10]


class TestFile(models.Model):
	file_name = models.CharField(max_length=255,blank=True,null=True)
	user_file_text  = models.TextField(blank=True,null=True)
	user_file = models.FileField()

	def __str__(self):
		return self.file_name




class Vocabulary(models.Model):
	course = models.ForeignKey('Course',on_delete=models.CASCADE)
	lesson = models.ForeignKey('Lesson',on_delete=models.CASCADE,blank=True,null=True)
	word = models.CharField(max_length=500)
	pos = models.CharField(max_length=50,blank=True,null=True)
	meaning = models.CharField(max_length=1000)
	def __str__(self):
		return self.word





class UserPreAssessmentStatus(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE)
	completed = models.BooleanField(default=False)

	def __str__(self):
		return str(self.user)



class UserPostAssessmentStatus(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	topic = models.ForeignKey('Topic',on_delete=models.CASCADE)
	completed = models.BooleanField(default=False)

	def __str__(self):
		return str(self.user)

#-----------------------             ---------------------           -----------------------




# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")
        user_obj = self.model(
            email = self.normalize_email(email),
            
        )
        user_obj.set_password(password) # change user password
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.is_active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email, password=None):
        user = self.create_user(
                email,
                
                password=password,
                is_staff=True
        )
        return user

    def create_superuser(self, email,  password=None):
        user = self.create_user(
                email,
               
                password=password,
                is_staff=True,
                is_admin=True
        )
        return user

class User(AbstractUser):
	PERSON_TYPE_CHOICES = (
	    ('Teacher', ('Teacher')),
	    ('Student', ('Student ')),
	)
	teacher_student = models.CharField(max_length=255,choices = PERSON_TYPE_CHOICES,blank=True,null=True)
	username = models.CharField(max_length=40, unique=False, default='')

	objects = UserManager()

	def __unicode__(self):
	    return self.teacher_student



class TeacherProfile(models.Model):
	user = models.ForeignKey(User,on_delete = models.CASCADE)
	student_auth_uuid  = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
	school = models.ForeignKey(School,on_delete=models.CASCADE, blank=True, null=True)
	course = models.ManyToManyField(Course,blank=True,null=True)
	#classes = models.ManyToManyField(TeacherClass,blank=True,null=True)
	verified = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):

		return self.user.username

	def get_absolute_url(self):

		return f"/{self.user.username}/dashboard"


class StudentProfile(models.Model):
	user = models.ForeignKey(User,on_delete = models.CASCADE)
	school = models.ForeignKey(School,on_delete=models.CASCADE, blank=True, null=True)
	course = models.ManyToManyField(Course,blank=True,null=True)
	teacher = models.ManyToManyField(TeacherProfile,blank=True,null=True)
	classes = models.ManyToManyField(TeacherClass,blank=True,null=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):

		return self.user.username


	def get_student_stats_url(self):
		return f"/statistics/{self.user.username}"




class TeacherCourseUUID(models.Model):
	teacher = models.ForeignKey(TeacherProfile,on_delete=models.CASCADE)
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	course_uuid = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.course_uuid)




from allauth.account.signals import user_logged_in

from allauth.account.signals import user_signed_up

from django.dispatch import receiver



@receiver(user_signed_up, dispatch_uid="some.unique.string.id.for.allauth.user_signed_up")
def user_signed_up_(request, user, **kwargs):
    # user signed up now send email
    # send email part - do your self
    if user.teacher_student == "Teacher":
    	instance = TeacherProfile()
    	instance.user = user
    	instance.save()
    elif user.teacher_student == "Student":
    	instance = StudentProfile()
    	instance.user = user
    	instance.save()

    print("request",request)
    print("user",user.teacher_student)






class UserLessonCompletionStatus(models.Model):
	student_profile = models.ForeignKey(StudentProfile,on_delete=models.CASCADE)
	lesson = models.ForeignKey(Lesson,on_delete=models.CASCADE)
	topic = models.ForeignKey(Topic,on_delete=models.CASCADE)
	course = models.ForeignKey(Course,on_delete=models.CASCADE,blank=True,null=True)
	topic_completed = models.BooleanField(default=False)
	lesson_completed = models.BooleanField(default=False)

	def __str__(self):
		return self.student_profile.user.email


def pre_save_receiver_course_model(sender,instance,*args,**kwargs):
	if instance.slug is None or instance.slug == '':
		instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_receiver_course_model,Course)

def pre_save_receiver_products_model(sender,instance,*args,**kwargs):
	if instance.slug is None or instance.slug == '':
		instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_receiver_products_model,Products)


def pre_save_receiver_lesson_content_model(sender,instance,*args,**kwargs):
	if instance.slug is None or instance.slug == '':
		instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_receiver_lesson_content_model,LessonContent)



def pre_save_receiver_teacher_class_model(sender,instance,*args,**kwargs):
	if instance.slug is None or instance.slug == '':
		instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_receiver_teacher_class_model,TeacherClass)






