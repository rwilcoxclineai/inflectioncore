# # -*- coding: utf-8 -*-
# from __future__ import unicode_literals

# from django.db import models

# # Create your models here.
# from django.contrib.auth import get_user_model

# from django.contrib.auth.models import AbstractUser

# from django.conf import settings

# User = settings.AUTH_USER_MODEL

# import uuid 

# from core.models import School,Course,Class


# class User(AbstractUser):
# 	PERSON_TYPE_CHOICES = (
# 	    ('Teacher', ('Teacher')),
# 	    ('Student', ('Student ')),
# 	)
# 	teacher_student = models.CharField(max_length=255,choices = PERSON_TYPE_CHOICES,blank=True,null=True)


# 	def __unicode__(self):
# 	    return self.teacher_student



# class TeacherProfile(models.Model):
# 	user = models.ForeignKey(User,on_delete = models.CASCADE)
# 	student_auth_uuid  = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
# 	school = models.ForeignKey(School,on_delete=models.CASCADE, blank=True, null=True)
# 	course = models.ManyToManyField(Course,blank=True,null=True)
# 	# classes = models.ManyToManyField(Class,blank=True,null=True)
# 	verified = models.BooleanField(default=False)
# 	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
# 	updated = models.DateTimeField(auto_now=True)

# 	def __str__(self):

# 		return self.user.username

# 	def get_absolute_url(self):

# 		return f"/{self.user.username}/dashboard"


# class StudentProfile(models.Model):
# 	user = models.ForeignKey(User,on_delete = models.CASCADE)
# 	school = models.ForeignKey(School,on_delete=models.CASCADE, blank=True, null=True)
# 	course = models.ManyToManyField(Course,blank=True,null=True)
# 	teacher = models.ManyToManyField(TeacherProfile,blank=True,null=True)
# 	classes = models.ManyToManyField(Class,blank=True,null=True)
# 	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
# 	updated = models.DateTimeField(auto_now=True)

# 	def __str__(self):

# 		return self.user.username



# class TeacherCourseUUID(models.Model):
# 	teacher = models.ForeignKey(TeacherProfile,on_delete=models.CASCADE)
# 	course = models.ForeignKey(Course,on_delete=models.CASCADE)
# 	course_uuid = models.UUIDField(default=uuid.uuid4, editable=False,unique=True)
# 	timestamp = models.DateTimeField(auto_now_add=True , auto_now=False)
# 	updated = models.DateTimeField(auto_now=True)

# 	def __str__(self):
# 		return str(self.course_uuid)




# from allauth.account.signals import user_logged_in

# from allauth.account.signals import user_signed_up

# from django.dispatch import receiver



# @receiver(user_signed_up, dispatch_uid="some.unique.string.id.for.allauth.user_signed_up")
# def user_signed_up_(request, user, **kwargs):
#     # user signed up now send email
#     # send email part - do your self
#     if user.teacher_student == "Teacher":
#     	instance = TeacherProfile()
#     	instance.user = user
#     	instance.save()
#     elif user.teacher_student == "Student":
#     	instance = StudentProfile()
#     	instance.user = user
#     	instance.save()

#     print("request",request)
#     print("user",user.teacher_student)
#     # print("session key",request.session.session_key)
#     # print("duplicate session key",request.session["session_key_duplicate"])
#     # if request.session["session_key_duplicate"]:
#     # 	income_sources = IncomeSources.objects.get(session_key = request.session["session_key_duplicate"])
#     # 	income_sources.user = user
#     # 	income_sources.save()









