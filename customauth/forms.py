from django import forms

from .models import *
from django.contrib.auth import get_user_model
User = get_user_model()


class SignupForm(forms.ModelForm):
	   # teacher_student = forms.CharField(max_length=10,label='phone_number')
	PERSON_TYPE_CHOICES = (
	    ('Teacher', ('Teacher')),
	    ('Student', ('Student ')),
	)
	teacher_student = forms.ChoiceField(choices=PERSON_TYPE_CHOICES, widget=forms.RadioSelect())
	class Meta:
		model = User
		fields = ['email','teacher_student']


 

	def signup(self, request, user):

		user.teacher_student = self.cleaned_data['teacher_student']
		user.save()




class TeacherVerificationForm(forms.Form):

	school_name = forms.CharField(label='School Name', max_length=500)
	school_uuid = forms.CharField(label='School ID',   max_length=500)


class StudentAddCourseForm(forms.Form):
	teacher_email =  forms.EmailField(label='Teacher Email')
	course_uuid = forms.CharField(label = 'Course ID')


class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['first_name','last_name']