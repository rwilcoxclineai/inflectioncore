from django.shortcuts import render

# Create your views here.
from django.http import HttpResponseRedirect
from django.contrib import messages
from core.models import *
from django.urls import reverse
#from .models import TeacherProfile,StudentProfile,TeacherCourseUUID
import uuid
from django.shortcuts import get_object_or_404,redirect

from core.forms import UserResponseForm,UserTextFreeForm,TestFileForm,TeacherVerificationForm,StudentAddCourseForm
import docx2txt

from .forms import *

def home(request):

	return redirect(reverse("account_login"))

	# form = TestFileForm(request.POST or None,request.FILES)
	# file_content = None
	# if form.is_valid():
	# 	print("inside form and request files is " , request.FILES['user_file'].name)

	# 	instance = form.save(commit=False)
	# 	instance.file_name  = request.FILES['user_file'].name
	# 	file_ = request.FILES['user_file']
	# 	text = docx2txt.process(file_)
	# 	instance.user_file_text = text
	# 	instance.save()


	context= {
		# "form":form,
		# "file_content":file_content,
	}
	return render(request,"base.html",context)


def user_profile(request):
	template = "profile.html"

	teacher_profile = None
	student_profile = None

	courses = Course.objects.all()

	form = UserForm(request.POST or None)
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")

	if request.method == "POST":
		if form.is_valid():
			current_user = User.objects.get(username = request.user.username)
			first_name = form.cleaned_data.get("first_name")
			last_name = form.cleaned_data.get("last_name")
			current_user.first_name  = first_name
			current_user.last_name = last_name
			current_user.save()
			messages.success(request,"Profile details saved successfully!")
			return redirect(reverse("teacher_student_modules"))
	

	context = {

	"form":form,

		# "teacher_profile": teacher_profile,
		# "student_profile": student_profile,
		# "courses": courses,
	}

	return render(request,template,context)


def teacher_student_verification(request):
	template = "teacher_student_verification.html"

	teacher_profile = None
	student_profile = None

	courses = Course.objects.all()
	try:
		teacher_profile = TeacherProfile.objects.get(user=request.user)
	except TeacherProfile.DoesNotExist:
		print("teacher does not exist")
		try:

			student_profile = StudentProfile.objects.get(user= request.user)
			print("Student exists")


		except StudentProfile.DoesNotExist:
			print("Student does not exist")
	

	context = {

		"teacher_profile": teacher_profile,
		"student_profile": student_profile,
		"courses": courses,
	}

	return render(request,template,context)


def verify_teacher(request):
	template = "teacher_verification.html"

	form = TeacherVerificationForm(request.POST or None)
	instance = TeacherProfile.objects.get(user =request.user)
	if instance.verified == True:
		messages.info(request, 'You have already verified your profile.')
		return HttpResponseRedirect(reverse('account_profile'))

	if request.method == "POST":

		if form.is_valid():
			print("Inside Valid Form")
			school_name = form.cleaned_data.get("school_name")
			school_uuid = form.cleaned_data.get("school_uuid")

			print("School name is",school_name)
			print("School uuid is",school_uuid)

			try:

				school_obj = School.objects.get(name=school_name, school_uuid = uuid.UUID(school_uuid))
				instance.course.add(Course.objects.get(slug="introduction-to-data-and-models"))
				instance.school = school_obj
				instance.verified = True
				instance.save()

				teacher_class = TeacherClass()
				teacher_class.course = Course.objects.get(slug="introduction-to-data-and-models")
				teacher_class.name = "First Class"
				teacher_class.teacher = instance
				teacher_class.save()
				print("Teacher Class is",teacher_class)

				messages.success(request, 'Profile Verified Successfully. Take note of the following details to share it with your students. Email: '+instance.user.email+' Class ID: '+str(teacher_class.class_uuid))
				return HttpResponseRedirect(reverse('teacher_student_modules'))
			except School.DoesNotExist:
				messages.error(request, "Could not find the school")

	else:
		form = TeacherVerificationForm()

	context = {

		"form": form,
	}

	return render(request,template,context)




def student_add_course(request):
	template = "student_add_course.html"

	form = StudentAddCourseForm(request.POST or None)

	if request.method == "POST":
		if form.is_valid():
			teacher_email = form.cleaned_data.get("teacher_email")
			class_uuid = form.cleaned_data.get("course_uuid")

			student_profile_instance = StudentProfile.objects.get(user = request.user)
			try:
				teacher_class = TeacherClass.objects.get(teacher__user__email = teacher_email, class_uuid = uuid.UUID(class_uuid))
				course_instance = Course.objects.get(name = teacher_class.course.name)
				if course_instance in student_profile_instance.course.all():
					messages.info(request,"You have already added this course. Got another UUID?")
					# return HttpResponseRedirect(reverse('teacher_modules'))
				else:
					student_profile_instance.course.add(Course.objects.get(name = teacher_class.course.name))
					student_profile_instance.teacher.add(TeacherProfile.objects.get(user__email = teacher_email))
					student_profile_instance.classes.add(teacher_class)
					student_profile_instance.save()
					messages.success(request,"Successfully added the course. Good luck!")
					return HttpResponseRedirect(reverse('teacher_student_modules'))

			except TeacherClass.DoesNotExist:
				messages.error(request, 'Error validating your details.')
			except ValueError:
				messages.error(request, 'Error validating your details.')




	else:
		form = StudentAddCourseForm()

	context = {

		"form": form,
	}

	return render(request,template,context)




def teacher_subscribe_course(request,course_uuid):

	template = "teacher_subscribe_course.html"
	print("The course uuid is",course_uuid)
	course = Course.objects.get(course_uuid = uuid.UUID(course_uuid))
	teacher_profile_instance = TeacherProfile.objects.get(user=request.user)
	teacher_profile_instance.course.add(course)

	instance,created = TeacherCourseUUID.objects.get_or_create(course = course,teacher=teacher_profile_instance)

	if created:
		print("created a new Teacher Course UUId object")
		messages.success(request, 'You have subscribed to the course successfully')
	else:
		print("Teacher Course UUID object already exists")
		messages.info(request, 'You have already subscribed to this course.')




	context = {
	"instance":instance,

	}

	return render(request,template,context)




def course_detail(request,course_uuid):
	template = "course_detail.html"

	course = Course.objects.get(course_uuid = uuid.UUID(course_uuid))
	teacher_profile_instance = TeacherProfile.objects.get(user=request.user)

	instance = TeacherCourseUUID.objects.get(course = course,teacher=teacher_profile_instance)



	context = {

	"course": course,
	"instance":instance,
	
	}

	return render(request,template,context)


def course_detail_student(request,course_uuid):
	template = "course_detail_student.html"

	course = Course.objects.get(course_uuid = uuid.UUID(course_uuid))
	student_profile_instance = StudentProfile.objects.get(user=request.user)


	questions = Question.objects.get_unanswered(request.user).order_by("?").first()
	if not questions:
		questions = None
	

	context = {

	"course": course,
	"questions":questions,
	
	}

	return render(request,template,context)


def single_question(request,id):
	template = "single_question.html"

	print("and the question id is", id)

	instance = get_object_or_404(Question, id=id)


	try:
		user_answer = UserAnswer.objects.get(user=request.user, question=instance)
		updated_q = True
	except UserAnswer.DoesNotExist:
		user_answer = UserAnswer()
		updated_q = False
	except UserAnswer.MultipleObjectsReturned:
		user_answer = UserAnswer.objects.filter(user=request.user, question=instance)[0]
		updated_q = True
	except:
		user_answer = UserAnswer()
		updated_q = False



	try:
		user_text_answer = UserTextAnswer.objects.get(user=request.user, question=instance)
		updated_user_text_answer = True
		initial_dict = {
		"my_answer": user_text_answer.my_answer.text
		}
	except UserTextAnswer.DoesNotExist:
		updated_user_text_answer = False
		user_text_answer = UserTextAnswer()
		initial_dict = {

		}
	except UserTextAnswer.MultipleObjectsReturned:
		user_text_answer = UserTextAnswer.objects.filter(user=request.user, question=instance)[0]
		updated_user_text_answer = True
		initial_dict = {
		"my_answer": user_text_answer.my_answer.text
		}
	except:
		user_text_answer = UserTextAnswer()
		updated_user_text_answer = False
		initial_dict = {

		}



	try:
		free_text_answer = FreeTextAnswer.objects.get(answers_id=instance.id)
	except FreeTextAnswer.DoesNotExist:
		free_text_answer = FreeTextAnswer()
	except FreeTextAnswer.MultipleObjectsReturned:
		free_text_answer = FreeTextAnswer.objects.filter(answers_id=instance.id)[0]
	except:
		free_text_answer = FreeTextAnswer()

		


	form2 = UserTextFreeForm(request.POST or None, initial = initial_dict)	


	form = UserResponseForm(request.POST or None)


	if form2.is_valid():
	
	
		question_id = form2.cleaned_data.get('question_id')
		user_text_input = form2.cleaned_data.get('my_answer')

		question_instance = Question.objects.get(id=question_id)
		
		
		free_text_answer.question = question_instance
		free_text_answer.text = user_text_input
		free_text_answer.save()

			
		user_text_answer.user = request.user
		user_text_answer.question = question_instance
		user_text_answer.my_answer = free_text_answer
		user_text_answer.save()
		
		next_q = Question.objects.get_unanswered(request.user).order_by("?")
		print ("The qs from form 2 next_q",next_q)
		if next_q.count() > 0:
			next_q_instance = next_q.first()
			return redirect("single_question",id=next_q_instance.id)
		else:
			return redirect("home")



	if form.is_valid():

		question_id = form.cleaned_data.get('question_id') #form.cleaned_data['question_id']
		answer_id = form.cleaned_data.get('answer_id')
		question_instance = Question.objects.get(id=question_id)
		answer_instance = Answer.objects.get(id=answer_id)

		if answer_instance.text == instance.correct_answer:
			user_answer.user = request.user
			user_answer.question = question_instance
			user_answer.my_answer = answer_instance
			user_answer.save()

			next_q = Question.objects.get_unanswered(request.user).order_by("?")
			print ("The qs from form 1 is", next_q)
			if next_q.count() > 0:

				next_q_instance = next_q.first()

				return redirect("single_question",id=next_q_instance.id)
			else:
				messages.success(request,"All answers have been answered.")
				return redirect("home")
		else:
			messages.error(request,"You submitted a wrong answer. You got one more chance,lets do it!")





	context = {
		"form": form,
		"instance": instance,
		"user_answer": user_answer,
		"form2": form2,
		"user_text_answer": user_text_answer,
		"free_text_answer": free_text_answer,
	}

	return render(request,template,context)





















