#!/bin/bash
cd /root/inflectiontest
nohup python3 manage.py runserver 0.0.0.0:8000 &> log.txt &
echo $! > pid.txt
